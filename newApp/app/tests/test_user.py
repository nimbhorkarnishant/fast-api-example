from ..main import app
from fastapi.testclient import TestClient
from fastapi import FastAPI
import requests
import pytest



# client = TestClient(app)

def test_root():
    response=requests.get("http://127.0.0.1:8000/api/testing")
    assert response.status_code == 200
    assert response.json() == {"message": "Running"}
    
    
def test_get_all_user():
    response =requests.get("http://127.0.0.1:8000/api/users")
    assert response.status_code ==200
    assert response.json() == {'status_code': 200, 'message': 'Success', 'user_data': [{'id': '5ffbfcd43d77a642b6f32088', 'f_name': 'Nishant', 'l_name': 'Nimbhorkar'}]}
    

    
def test_get_user_by_Id():
    response = requests.get("http://127.0.0.1:8000/api/user/5ffbfcd43d77a642b6f32088")
    assert response.status_code == 200
    assert response.json() == {'status_code': 200, 'message': 'Success', 'user_data': {'id': '5ffbfcd43d77a642b6f32088', 'f_name': 'Nishant', 'l_name': 'Nimbhorkar'}}
    