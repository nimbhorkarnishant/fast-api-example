from typing import Optional
from fastapi import APIRouter, Body, Depends, Path, Query
from slugify import slugify
from bson.objectid import ObjectId
from starlette.exceptions import HTTPException
from starlette.status import (
    HTTP_201_CREATED,
    HTTP_204_NO_CONTENT,
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_422_UNPROCESSABLE_ENTITY,
)
from ....core.config import database_name, user_collection
from ....db.mongodb import AsyncIOMotorClient,get_database
from ....model.user import User
from fastapi.encoders import jsonable_encoder


router = APIRouter()



def user_helper(user) -> dict:
    return {
        "id": str(user["_id"]),
        "f_name": user["f_name"],
        "l_name": user["l_name"],
    }

    

@router.get("/testing")
async def root():
    return {"message": "Running"}
    
    
@router.get("/")
async def get_user_count(db: AsyncIOMotorClient = Depends(get_database)):
    try:
        count=await db[database_name][user_collection].count_documents({})
        return {"status_code":200,"message":"Success","user_data":count}
    except:
        return {"status_code":400,"message":"Something went wrong!"}


@router.get("/users")
async def get_all_user(db: AsyncIOMotorClient = Depends(get_database)):
    try:
        data=db[database_name][user_collection].find()
        # data= db.get_collection("user_data")
        print(data)
        arr=[] 
        for document in await data.to_list(length=100):
            print(document)
            arr.append(user_helper(document))
        print("hillllllllllllllllll")
        return {"status_code":200,"message":"Success","user_data":arr  }
    except:
        return {"status_code":500,"message":"User Not Found with this Id"}
    
@router.get("/user/{id}")
async def get_user_by_Id(id: str, q: Optional[str] = None,db: AsyncIOMotorClient = Depends(get_database)):
    try:
        # data_doc=db.get_collection("user_data")
        # user_data =data_doc.find_one({"_id": ObjectId(id)})
        user_data=await db[database_name][user_collection].find_one({"_id": ObjectId(id)})
        print(user_data)
        return {"status_code":200,"message":"Success","user_data":user_helper(user_data)}
    except:
        return {"status_code":500,"message":"User Not Found with this Id"}
    
@router.post("/add_user/")
async def add_user(user:User,db: AsyncIOMotorClient = Depends(get_database)):
    json_data = jsonable_encoder(user)

    result = await db[database_name][user_collection].insert_one(json_data)
    return {"status_code":200,"message":"User Added Successfully","user_data":str(repr(result.inserted_id))}
