from typing import Optional

from fastapi import FastAPI
from pydantic import BaseModel


class User(BaseModel):
    f_name: str
    l_name: str